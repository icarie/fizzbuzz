package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest
import io.mockk.every
import io.mockk.mockk
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class DefaultMostUsedRequestRetrieverTest : TestCase() {

    private lateinit var mostUsedRequestRetriever: DefaultMostUsedRequestRetriever

    @Test
    fun testMostUsedRequestReturnsNullWithEmptyList() {
        val userRequestManager = mockk<UserRequestManager>()
        every { userRequestManager.retrieveRequests() } returns listOf()

        mostUsedRequestRetriever = DefaultMostUsedRequestRetriever(userRequestManager)

        assertTrue(mostUsedRequestRetriever.getMostUsedRequest() == null)
    }

    @Test
    fun testMostUsedRequestReturnsRightResultWithValidList() {
        val userRequestManager = mockk<UserRequestManager>()
        every { userRequestManager.retrieveRequests() } returns getValidList()

        mostUsedRequestRetriever = DefaultMostUsedRequestRetriever(userRequestManager)

        assert(mostUsedRequestRetriever.getMostUsedRequest() != null)
        assert(mostUsedRequestRetriever.getMostUsedRequest()?.userRequest == getSecondUserRequest())
    }

    private fun getValidList(): List<UserRequest> {
        val list: MutableList<UserRequest> = mutableListOf()
        list.add(getSecondUserRequest())
        list.add(getFirstUserRequest())
        list.add(getSecondUserRequest())
        list.add(getThirdUserRequest())
        list.add(getSecondUserRequest())
        return list
    }

    private fun getSecondUserRequest() = UserRequest(10, 2, 5, "toto", "tata")

    private fun getThirdUserRequest() = UserRequest(1000, 20, 5, "pipo", "papi")

    private fun getFirstUserRequest() = UserRequest(100, 24, 52, "mdr", "ok")
}