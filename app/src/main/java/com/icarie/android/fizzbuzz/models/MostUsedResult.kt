package com.icarie.android.fizzbuzz.models

data class MostUsedResult(val userRequest: UserRequest, val requestNumber : Int)