package com.icarie.android.fizzbuzz.usecases

import io.reactivex.rxjava3.core.Observable

interface GenerateSequence {
    fun generate(limit: Int?,
                 firstModulo: Int?,
                 secondModulo: Int?,
                 firstSubstitute: String?,
                 secondSubstitute: String?): Observable<List<String>>
}