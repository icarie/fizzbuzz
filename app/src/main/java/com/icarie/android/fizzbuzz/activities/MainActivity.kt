package com.icarie.android.fizzbuzz.activities

import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.icarie.android.fizzbuzz.R
import com.icarie.android.fizzbuzz.databinding.ActivityMainBinding
import com.icarie.android.fizzbuzz.services.KeyboardService
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val keyboardService: KeyboardService by inject()

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)?.apply {
            initNavController(bottomNavigation)
        }
    }

    private fun initNavController(bottomNavigation: BottomNavigationView) {
        Navigation.findNavController(this@MainActivity, R.id.nav_host_fragment).let {
            navController = it
            NavigationUI.setupWithNavController(bottomNavigation, it)
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        currentFocus?.let {
            keyboardService.close(this, it)
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()
}
