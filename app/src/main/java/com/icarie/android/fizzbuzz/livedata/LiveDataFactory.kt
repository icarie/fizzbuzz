package com.icarie.android.fizzbuzz.livedata

import androidx.lifecycle.LiveData
import com.icarie.android.fizzbuzz.models.MostUsedResult

interface LiveDataFactory {
    fun getMostUsedRequestLiveData(): LiveData<MostUsedResult>
}