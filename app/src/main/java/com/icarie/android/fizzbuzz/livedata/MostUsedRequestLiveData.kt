package com.icarie.android.fizzbuzz.livedata

import androidx.lifecycle.MutableLiveData
import com.icarie.android.fizzbuzz.models.MostUsedResult
import com.icarie.android.fizzbuzz.usecases.RetrieveMostUsedRequest

class MostUsedRequestLiveData(private val retrieveMostUsedRequest: RetrieveMostUsedRequest) :
    MutableLiveData<MostUsedResult>() {

    // using lifecycle aware object to update ui
    override fun onActive() {
        super.onActive()
        postValue(retrieveMostUsedRequest.getRequest())
    }
}
