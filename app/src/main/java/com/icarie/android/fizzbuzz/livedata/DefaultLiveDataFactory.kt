package com.icarie.android.fizzbuzz.livedata

import androidx.lifecycle.LiveData
import com.icarie.android.fizzbuzz.models.MostUsedResult
import com.icarie.android.fizzbuzz.usecases.RetrieveMostUsedRequest
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class DefaultLiveDataFactory : LiveDataFactory, KoinComponent {

    override fun getMostUsedRequestLiveData(): LiveData<MostUsedResult> {
        val retrieveMostUsedRequest: RetrieveMostUsedRequest by inject()
        return MostUsedRequestLiveData(retrieveMostUsedRequest)
    }
}