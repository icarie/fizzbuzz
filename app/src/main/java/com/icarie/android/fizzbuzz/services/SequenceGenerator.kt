package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest
import io.reactivex.rxjava3.core.Observable

interface SequenceGenerator {
    fun generate(userRequest : UserRequest): Observable<List<String>>
}
