package com.icarie.android.fizzbuzz.usecases

import com.icarie.android.fizzbuzz.models.MostUsedResult

interface RetrieveMostUsedRequest {
    fun getRequest(): MostUsedResult?
}