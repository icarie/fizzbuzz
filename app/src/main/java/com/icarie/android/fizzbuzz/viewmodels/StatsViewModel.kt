package com.icarie.android.fizzbuzz.viewmodels

import com.icarie.android.fizzbuzz.livedata.LiveDataFactory
import com.icarie.android.fizzbuzz.viewmodels.ObservableViewModel

class StatsViewModel(liveDataFactory: LiveDataFactory) : ObservableViewModel() {

    val mostUsedRequestLiveData = liveDataFactory.getMostUsedRequestLiveData()
}