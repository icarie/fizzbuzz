package com.icarie.android.fizzbuzz.models

data class UserRequest(val limit : Int,
                       val firstModulo : Int,
                       val secondModulo : Int,
                       val firstSubstitute : String,
                       val secondSubstitute : String) {

    override fun toString(): String {
        return "UserRequest(limit=$limit, firstModulo=$firstModulo, " +
                "secondModulo=$secondModulo, firstSubstitute='$firstSubstitute', secondSubstitute='$secondSubstitute')"
    }
}