package com.icarie.android.fizzbuzz.applications

import androidx.multidex.MultiDexApplication
import com.icarie.android.fizzbuzz.BuildConfig
import com.icarie.android.fizzbuzz.dependancyinjection.DIModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.component.KoinApiExtension
import org.koin.core.context.startKoin
import timber.log.Timber

class FizzBuzzApplication : MultiDexApplication() {

    @KoinApiExtension
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidLogger()
            androidContext(this@FizzBuzzApplication)
            modules(DIModule().getModule())
        }
    }
}