package com.icarie.android.fizzbuzz.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.icarie.android.fizzbuzz.databinding.FragmentFormBinding
import com.icarie.android.fizzbuzz.viewmodels.FormViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class FormFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentFormBinding.inflate(inflater).apply {
            val formViewModel : FormViewModel by viewModel()
            viewModel = formViewModel
        }.root
    }
}