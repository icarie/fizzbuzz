package com.icarie.android.fizzbuzz.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.icarie.android.fizzbuzz.databinding.FragmentStatsBinding
import com.icarie.android.fizzbuzz.viewmodels.StatsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class StatsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentStatsBinding.inflate(inflater).apply {
            val statsViewModel : StatsViewModel by viewModel()
            viewModel = statsViewModel
            lifecycleOwner = this@StatsFragment
        }.root
    }
}