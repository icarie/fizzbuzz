package com.icarie.android.fizzbuzz.usecases

import com.icarie.android.fizzbuzz.models.MostUsedResult
import com.icarie.android.fizzbuzz.services.MostUsedRequestRetriever

class DefaultRetrieveMostUsedRequest(private val mostUsedRequestRetriever: MostUsedRequestRetriever) :
    RetrieveMostUsedRequest {

    override fun getRequest(): MostUsedResult? = mostUsedRequestRetriever.getMostUsedRequest()
}