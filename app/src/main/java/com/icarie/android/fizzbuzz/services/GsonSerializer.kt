package com.icarie.android.fizzbuzz.services

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.icarie.android.fizzbuzz.models.UserRequest
import java.lang.reflect.Type

class GsonSerializer(private val gson: Gson) : Serializer {

    override fun serialize(userRequest: List<UserRequest>): String = gson.toJson(userRequest).toString()

    override fun deSerialize(json: String): List<UserRequest> {
        val type: Type? = object : TypeToken<List<UserRequest>>() {}.type
        return type?.let { gson.fromJson(json, it) } ?: listOf()
    }
}