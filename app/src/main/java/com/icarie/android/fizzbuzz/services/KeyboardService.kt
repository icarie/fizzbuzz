package com.icarie.android.fizzbuzz.services

import android.content.Context
import android.view.View

interface KeyboardService {
    fun show(context: Context, view: View)
    fun close(context: Context, view: View)
}