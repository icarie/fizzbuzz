package com.icarie.android.fizzbuzz.viewmodels

import androidx.databinding.Bindable
import androidx.databinding.Observable
import com.icarie.android.fizzbuzz.BR
import com.icarie.android.fizzbuzz.view.FizzBuzzForm
import com.icarie.android.fizzbuzz.services.SequenceValidator
import com.icarie.android.fizzbuzz.usecases.GenerateSequence
import com.icarie.android.fizzbuzz.usecases.ValidateSequence
import io.reactivex.rxjava3.disposables.Disposable
import timber.log.Timber

class FormViewModel(private val validateSequence: ValidateSequence,
                    private val generateSequence: GenerateSequence) : ObservableViewModel() {

    private var generatorDisposable : Disposable? = null

    val form: FizzBuzzForm = FizzBuzzForm()

    @Bindable
    var error: Boolean = false
        private set(value) {
            field = value
            notifyPropertyChanged(BR.error)
        }

    @Bindable
    var generating: Boolean = false
        private set(value) {
            field = value
            notifyPropertyChanged(BR.generating)
        }

    @Bindable
    var output: List<String> = listOf()
        private set(value) {
            field = value
            notifyPropertyChanged(BR.output)
            notifyPropertyChanged(BR.outputValid)
        }

    init {
        form.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                notifyPropertyChanged(BR.generationAllowed)
            }
        })
    }

    @Bindable
    fun isOutputValid() : Boolean = !output.isNullOrEmpty()

    @Bindable
    fun isGenerationAllowed(): Boolean {
        return validateSequence.isValid(form.limit, form.firstModulo, form.secondModulo,
            form.firstSubstitute, form.secondSubstitute) && !generating
    }

    fun generate() {
        if (isGenerationAllowed()) {
            dispose()
            generatorDisposable = generateSequence.generate(form.limit, form.firstModulo,
                form.secondModulo, form.firstSubstitute, form.secondSubstitute)
                .doOnSubscribe { generating = true; error = false }
                .doOnComplete { generating = false }
                .doOnError { generating = false; error = true }
                .subscribe({ output = it }, Timber::e)
        } else {
            Timber.e("Probable regression happening with form validation")

            // Not really user friendly to throw a crash but reaching that point means form validation is not working properly
            // throw InvalidParameterException("Should not be possible to request generation with invalid params")
        }
    }

    override fun onCleared() {
        super.onCleared()
        dispose()
    }

    private fun dispose() {
        generatorDisposable?.dispose()
        generatorDisposable = null
    }
}