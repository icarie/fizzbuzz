package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest

interface SequenceValidator {
    fun hasValidParameters(userRequest: UserRequest): Boolean
}