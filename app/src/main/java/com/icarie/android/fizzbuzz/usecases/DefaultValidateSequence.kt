package com.icarie.android.fizzbuzz.usecases

import com.icarie.android.fizzbuzz.models.UserRequest
import com.icarie.android.fizzbuzz.services.SequenceValidator
import com.icarie.android.fizzbuzz.services.UserRequestMapper

class DefaultValidateSequence(private val sequenceValidator: SequenceValidator,
                              private val userRequestMapper: UserRequestMapper) : ValidateSequence {

    override fun isValid(limit: Int?,
                         firstModulo: Int?,
                         secondModulo: Int?,
                         firstSubstitute: String?,
                         secondSubstitute: String?): Boolean {
        val userRequest : UserRequest = userRequestMapper.mapToUserRequest(limit, firstModulo,
            secondModulo, firstSubstitute, secondSubstitute)
        return sequenceValidator.hasValidParameters(userRequest)
    }
}