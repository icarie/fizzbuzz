package com.icarie.android.fizzbuzz.view

import androidx.databinding.Bindable
import com.icarie.android.fizzbuzz.BR
import com.icarie.android.fizzbuzz.viewmodels.ObservableViewModel

class FizzBuzzForm : ObservableViewModel() {

    @get:Bindable
    var limit: Int? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.limit)
        }

    @get:Bindable
    var firstModulo: Int? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.firstModulo)
        }

    @get:Bindable
    var secondModulo: Int? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.secondModulo)
        }

    @get:Bindable
    var firstSubstitute: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.firstSubstitute)
        }

    @get:Bindable
    var secondSubstitute: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.secondSubstitute)
        }
}