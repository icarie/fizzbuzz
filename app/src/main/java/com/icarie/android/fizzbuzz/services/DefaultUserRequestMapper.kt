package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest

private const val FALLBACK_NUMBER = -1
private const val FALLBACK_STRING = ""

class DefaultUserRequestMapper : UserRequestMapper {

    override fun mapToUserRequest(limit: Int?,
                                  firstModulo: Int?,
                                  secondModulo: Int?,
                                  firstSubstitute: String?,
                                  secondSubstitute: String?) : UserRequest {
        return UserRequest(
            getSafeNumber(limit),
            getSafeNumber(firstModulo),
            getSafeNumber(secondModulo),
            getSafeString(firstSubstitute),
            getSafeString(secondSubstitute))
    }

    private fun getSafeNumber(number: Int?): Int = number ?: FALLBACK_NUMBER

    private fun getSafeString(substitute: String?): String = substitute ?: FALLBACK_STRING
}