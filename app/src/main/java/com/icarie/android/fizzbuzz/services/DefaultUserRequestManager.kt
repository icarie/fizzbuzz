package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest

private const val CACHE_KEY = "cache"

class DefaultUserRequestManager(private val settingsService: SettingsService,
                                private val serializer: Serializer) : UserRequestManager {

    override fun saveRequest(userRequest: UserRequest) {
        val list : List<UserRequest> = requestDeSerialization()
        val mutableList : MutableList<UserRequest> = list.toMutableList()
        mutableList.add(userRequest)
        serializer.serialize(mutableList).let { settingsService.setString(CACHE_KEY, it) }
    }

    override fun retrieveRequests(): List<UserRequest> = requestDeSerialization()

    private fun requestDeSerialization() = serializer.deSerialize(settingsService.getString(CACHE_KEY, ""))
}