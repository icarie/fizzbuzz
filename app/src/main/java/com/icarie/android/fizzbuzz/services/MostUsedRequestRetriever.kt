package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.MostUsedResult

interface MostUsedRequestRetriever {

    // null if none
    fun getMostUsedRequest() : MostUsedResult?
}