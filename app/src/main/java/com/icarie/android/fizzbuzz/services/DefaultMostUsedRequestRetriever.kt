package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.MostUsedResult

class DefaultMostUsedRequestRetriever(private val userRequestManager: UserRequestManager) :
    MostUsedRequestRetriever {

    override fun getMostUsedRequest(): MostUsedResult? {
        return userRequestManager.retrieveRequests()
            .groupBy { it }
            .maxByOrNull { it.value.size }?.let { MostUsedResult(it.key, it.value.size) }
    }
}