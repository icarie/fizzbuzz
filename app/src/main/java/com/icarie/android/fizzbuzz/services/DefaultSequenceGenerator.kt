package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.exception.InvalidParametersException
import com.icarie.android.fizzbuzz.models.UserRequest
import io.reactivex.rxjava3.core.Observable
import timber.log.Timber

private const val START_SEQUENCE = 1

class DefaultSequenceGenerator(private val sequenceValidator: SequenceValidator) :
    SequenceGenerator {

    override fun generate(userRequest: UserRequest): Observable<List<String>> {
        return if (sequenceValidator.hasValidParameters(userRequest)) {
            startGenerationAndEmit(userRequest)
        } else {
            Observable.error(InvalidParametersException("Invalid parameters provided"))
        }
    }

    private fun startGenerationAndEmit(userRequest: UserRequest): Observable<List<String>> {
        return Observable.create { emiter ->
            mutableListOf<String>().apply {
                (START_SEQUENCE..userRequest.limit).iterator().forEach { currentNumber ->
                    add(formatIfNeeded(currentNumber, userRequest))
                }
            }.also {
                emiter.onNext(it)
                emiter.onComplete()
            }.also {
                Timber.d("generated output: $it")
            }
        }
    }

    private fun formatIfNeeded(currentNumber: Int, userRequest: UserRequest): String {
        return when {
            currentNumber % (userRequest.firstModulo * userRequest.secondModulo) == 0 -> {
                "${userRequest.firstSubstitute}${userRequest.secondSubstitute}"
            }
            currentNumber % userRequest.firstModulo == 0 -> userRequest.firstSubstitute
            currentNumber % userRequest.secondModulo == 0 -> userRequest.secondSubstitute
            else -> "$currentNumber"
        }
    }
}