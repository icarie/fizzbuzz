package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest

private const val START_SEQUENCE = 1

class DefaultSequenceValidator : SequenceValidator {

    override fun hasValidParameters(userRequest: UserRequest): Boolean {
        return hasValidParameters(userRequest.limit,
            userRequest.firstModulo,
            userRequest.secondModulo,
            userRequest.firstSubstitute,
            userRequest.secondSubstitute)
    }

    private fun hasValidParameters(limit: Int?,
                                   firstNumber: Int?,
                                   secondNumber: Int?,
                                   firstSubstitute: String?,
                                   secondSubstitute: String?): Boolean {
        return isLimitValid(limit)
                && isNumberInRange(firstNumber, limit)
                && isNumberInRange(secondNumber, limit)
                && !firstSubstitute.isNullOrEmpty()
                && !secondSubstitute.isNullOrEmpty()
    }

    private fun isLimitValid(limit: Int?) = limit != null && limit > START_SEQUENCE

    private fun isNumberInRange(number: Int?, limit: Int?) : Boolean {
        return limit?.let { safeLimit ->
            number != null && (number in START_SEQUENCE..safeLimit)
        } ?: false
    }
}