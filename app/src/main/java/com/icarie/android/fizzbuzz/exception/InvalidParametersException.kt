package com.icarie.android.fizzbuzz.exception

import kotlin.Exception

class InvalidParametersException(message: String) : Exception(message)