package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest

interface UserRequestMapper {
    fun mapToUserRequest(limit: Int?,
                         firstModulo: Int?,
                         secondModulo: Int?,
                         firstSubstitute: String?,
                         secondSubstitute: String?) : UserRequest
}