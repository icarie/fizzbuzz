package com.icarie.android.fizzbuzz.dependancyinjection

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.google.gson.Gson
import com.icarie.android.fizzbuzz.livedata.DefaultLiveDataFactory
import com.icarie.android.fizzbuzz.livedata.LiveDataFactory
import com.icarie.android.fizzbuzz.services.*
import com.icarie.android.fizzbuzz.usecases.*
import com.icarie.android.fizzbuzz.viewmodels.StatsViewModel
import com.icarie.android.fizzbuzz.viewmodels.FormViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.module.Module
import org.koin.dsl.module

private const val SHARED_PREFERENCES_NAME = "FizzBuzz"

class DIModule {

    @KoinApiExtension
    fun getModule(): Module {

        return module {

            // General stuff
            single<KeyboardService> { AndroidKeyboardService() }
            single<LiveDataFactory> { DefaultLiveDataFactory() }

            // Sequence generation
            single<SequenceValidator> { DefaultSequenceValidator() }
            single<SequenceGenerator> { DefaultSequenceGenerator(get()) }

            // Cache & stats
            single<Serializer> { GsonSerializer(Gson()) }
            single<SettingsService> { initSettingsService(get()) }
            single<UserRequestManager> { DefaultUserRequestManager(get(), get()) }
            single<MostUsedRequestRetriever> { DefaultMostUsedRequestRetriever(get()) }
            single<UserRequestMapper> { DefaultUserRequestMapper() }

            // UseCases
            single<GenerateSequence> { DefaultGenerateSequence(get(), get(), get()) }
            single<RetrieveMostUsedRequest> { DefaultRetrieveMostUsedRequest(get()) }
            single<ValidateSequence> { DefaultValidateSequence(get(), get()) }

            viewModel { FormViewModel(get(), get()) }

            viewModel { StatsViewModel(get()) }
        }
    }

    private fun initSettingsService(application: Application): AndroidSettingsService {
        val sharedPreferences = application.getSharedPreferences(SHARED_PREFERENCES_NAME, MultiDexApplication.MODE_PRIVATE)
        return AndroidSettingsService(sharedPreferences)
    }
}