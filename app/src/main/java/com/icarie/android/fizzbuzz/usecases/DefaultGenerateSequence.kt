package com.icarie.android.fizzbuzz.usecases

import com.icarie.android.fizzbuzz.services.SequenceGenerator
import com.icarie.android.fizzbuzz.services.UserRequestManager
import com.icarie.android.fizzbuzz.services.UserRequestMapper
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class DefaultGenerateSequence(private val sequenceGenerator: SequenceGenerator,
                              private val userRequestMapper: UserRequestMapper,
                              private val requestManager: UserRequestManager) : GenerateSequence {

    override fun generate(limit: Int?,
                          firstModulo: Int?,
                          secondModulo: Int?,
                          firstSubstitute: String?,
                          secondSubstitute: String?): Observable<List<String>> {
        val request = userRequestMapper.mapToUserRequest(limit, firstModulo, secondModulo,
            firstSubstitute, secondSubstitute)
        return sequenceGenerator.generate(request)
            .subscribeOn(Schedulers.computation())
            .observeOn(Schedulers.computation())
            .doOnNext { requestManager.saveRequest(request) }
    }
}