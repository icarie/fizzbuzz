package com.icarie.android.fizzbuzz.services

interface SettingsService {
    fun getBoolean(key: String, defaultValue: Boolean): Boolean
    fun setBoolean(key: String, value: Boolean)

    fun getString(key: String, defaultValue : String) : String
    fun setString(key: String, value : String)
}