package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest

interface Serializer {
    fun serialize(userRequest: List<UserRequest>) : String
    fun deSerialize(json : String) : List<UserRequest>
}