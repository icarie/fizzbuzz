package com.icarie.android.fizzbuzz.services

import com.icarie.android.fizzbuzz.models.UserRequest

interface UserRequestManager {
    fun saveRequest(userRequest: UserRequest)
    fun retrieveRequests() : List<UserRequest>
}
