package com.icarie.android.fizzbuzz.usecases

interface ValidateSequence {
    fun isValid(limit: Int?,
                 firstModulo: Int?,
                 secondModulo: Int?,
                 firstSubstitute: String?,
                 secondSubstitute: String?): Boolean
}